(use-modules (gnu)
             (gnu system nss))

(use-service-modules xorg
                     desktop)

(use-package-modules certs
                     gnome)

(operating-system
 (host-name "relativelytreeless")
 (timezone "Europe/London")
 (locale "en_GB.UTF-8")
 (keyboard-layout (keyboard-layout "us" "altgr-intl"))

 (bootloader
  (bootloader-configuration
   (bootloader (bootloader (inherit grub-bootloader)
                           (installer #~(const #t))))
   (keyboard-layout keyboard-layout)))

 (mapped-devices
  (list (mapped-device (source "/dev/sda1")
                       (target "crypt")
                       (type luks-device-mapping))))

 (file-systems
  (cons* (file-system (device (file-system-label "tales-from-the-crypt"))
                      (mount-point "/")
                      (type "btrfs")
                      (dependencies mapped-devices))
         %base-file-systems))
 (users
  (cons* (user-account (name "blueberry")
                       (comment "Amber G")
                       (group "users")
                       (supplementary-groups '("wheel"
                                               "netdev"
                                               "audio"
                                               "video"
                                               "lp"
                                               "cdrom"
                                               "tape"
                                               "kvm")))
         %base-user-accounts))

 (packages (cons* nss-certs
                  %base-packages))

 (services (cons* (extra-special-file
                   "/usr/bin/env"
                   (file-append coreutils "/bin/env"))

                  (set-xorg-configuration
                   (xorg-configuration
                    (keyboard-layout keyboard-layout)))

                  (service gnome-desktop-service-type)
                  %desktop-services))

 (name-service-switch %mdns-host-lookup-nss))
